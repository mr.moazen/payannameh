

function drawOurGoalsChart() {
    var ChartWinRate = new Array();
    console.log(goalzade)
    ChartWinRate[0] = ['JobID', 'Goals'];
    var i =1 ;
    $.each(goalzade, function( index, value ) { 
        ChartWinRate[i]=[index,value];
        i++
        });
    var data = google.visualization.arrayToDataTable(ChartWinRate ,false);

    var options = {
        title: 'Our  Goals ',
        colors: ['#04ff00'],
        lineWidth :4,
        pointSize: 16, 
        hAxis: {
            title: 'Job ID Number'  ,
            titleTextStyle :{
                bold :true,
                color: 'black'
            }
          },
          vAxis: {
            title: 'Our goals Avg',
            titleTextStyle :{
                bold :true,
                color: 'black'
            }
          },
        curveType: 'function',
        legend: {
            position: 'bottom'
        }
    };

    var chart = new google.visualization.AreaChart(document.getElementById('Avggoalzade'));

    chart.draw(data, options);
}
